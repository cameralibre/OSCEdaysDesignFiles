These images are free culture. You may use, modify, and publish these files as long as you credit the author, which you can do like this:

'Online Exchange' by Jenni Ottilie Keppler (http://ottilie.cc) is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

'Workshops' by Jenni Ottilie Keppler (http://ottilie.cc) is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)



You may use, modify, and publish these files as long as you credit the author and release any changes/derivative works under the same CC-BY-SA license, which you can do like this:

'OSCEdays World Map' by Sam Muirhead (http://cameralibre.cc) is released under a Creative Commons Attribution-ShareAlike 4.0 International License 
(https://creativecommons.org/licenses/by-sa/4.0/)

'Circular Economy' by Sam Muirhead (http://cameralibre.cc) is released under a Creative Commons Attribution-ShareAlike 4.0 International License 
(https://creativecommons.org/licenses/by-sa/4.0/)

'Linear Economy' by Sam Muirhead (http://cameralibre.cc) is released under a Creative Commons Attribution-ShareAlike 4.0 International License 
(https://creativecommons.org/licenses/by-sa/4.0/)

'Tree Life Cycle (complex)' by Sam Muirhead (http://cameralibre.cc) is released under a Creative Commons Attribution-ShareAlike 4.0 International License 
(https://creativecommons.org/licenses/by-sa/4.0/)

'Open Source Everything' by Sam Muirhead (http://cameralibre.cc) is released under a Creative Commons Attribution-ShareAlike 4.0 International License 
(https://creativecommons.org/licenses/by-sa/4.0/)

'Tree Life Cycle (simple)' by Sam Muirhead (http://cameralibre.cc) is released under a Creative Commons Attribution-ShareAlike 4.0 International License 
(https://creativecommons.org/licenses/by-sa/4.0/)
