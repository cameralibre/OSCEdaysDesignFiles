These images are free culture. You may use, modify, and publish these files as long as you credit the author, which you can do like this:

'OSCEdays Berlin 2016' by Jenni Ottilie Keppler (http://ottilie.cc) is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)


