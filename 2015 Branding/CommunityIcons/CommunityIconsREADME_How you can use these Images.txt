These images came from Noun Project.(http://thenounproject.com) 
It's just wonderful.

These are licensed under a free cultural copyright license. You may use, modify, and publish these files as long as you credit the author, which you can do like this:

Globe by Proletkult Graphik on The Noun Project
https://thenounproject.com/term/globe/1695/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

Lightbulb by Kevin Bernett on The Noun Project
https://thenounproject.com/term/light-bulb/51586/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

Rubiks Cube by David Papworth on The Noun Project
https://thenounproject.com/term/rubiks-cube/18791/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

Donors by Martha Ormiston on The Noun Project
https://thenounproject.com/term/donors/66584/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

Chat by Bruno on The Noun Project
https://thenounproject.com/term/chat/24623/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

Network by Brennan Novak on The Noun Project
https://thenounproject.com/term/network/21268/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

Map Marker by OliM on The Noun Project
https://thenounproject.com/term/map-marker/51274/
is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

