These images are free culture. You may use, modify, and publish these files as long as you credit the author, which you can do like this:

'City_Paris' by Jenni Ottilie Keppler (http://ottilie.cc) is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

'City_Berlin' by Jenni Ottilie Keppler (http://ottilie.cc) is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

'City_London' by Jenni Ottilie Keppler (http://ottilie.cc) is released under a Creative Commons Attribution 4.0 International License 
(https://creativecommons.org/licenses/by/4.0/)

'City_Rotterdam' by Freepik (http://www.flaticon.com/authors/freepik) and Peter Troxler is released under a Creative Commons Attribution 3.0 International License (https://creativecommons.org/licenses/by/3.0/)
