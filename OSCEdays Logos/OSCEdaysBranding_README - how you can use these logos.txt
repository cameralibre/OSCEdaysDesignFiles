These logos are licensed under a free cultural copyright license. 
You may use, modify, and publish these files as long as you credit the author, which you can do like this:

'Open Source Circular Economy Days' by Jenni Ottilie Keppler (http://ottilie.cc) 
is released under a Creative Commons Attribution 4.0 International License (https://creativecommons.org/licenses/by/4.0/)

'OSCEdays Logo' by Jenni Ottilie Keppler (http://ottilie.cc) and Max Fabian (http://maxfabian.org) 
is released under a Creative Commons Attribution 4.0 International License (https://creativecommons.org/licenses/by/4.0/)

The logos may be adapted to reflect a local group or event, but please try to ensure that they are recognisable as OSCEdays, 
and we would prefer it if you used the OSCEdays colour scheme: (https://cloud.oscedays.org/index.php/s/218v11m3yLFGNbp)

HOWEVER:
Logos are covered by more than just copyright.
They come under trademark law, but most importantly, the OSCEdays brand represents this community, its commitment to the open source methodology and the goal of a circular economy.
Before using any OSCEdays branding, ensure that you have read and that you agree to the guidelines and principles of the OSCEdays, as outlined in our README document: (https://oscedays.org/oscedays-global-read-me/)

The OSCEdays organizing team reserves the right to deny others use of the OSCEdays brand in specific cases where the community feels that the guidelines and principles of the Open Source Circular Economy Days are not being upheld.
